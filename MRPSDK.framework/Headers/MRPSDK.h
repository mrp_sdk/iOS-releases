//
//  MRPSDK.h
//  MRPSDK
//
//  Created by Dimitri Dupuis-Latour on 29/06/2018.
//  Copyright © 2018 DDL Consulting Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MRPSDK.
FOUNDATION_EXPORT double MRPSDKVersionNumber;

//! Project version string for MRPSDK.
FOUNDATION_EXPORT const unsigned char MRPSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MRPSDK/PublicHeader.h>


